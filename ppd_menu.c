/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* menu.h gives indirect access to ALL of the header files in the program */
#include "ppd_menu.h"

/**
 * @file ppd_menu.c handles the initialised and management of the menu 
 * array
 **/

/**
 * @param menu the menu item array to initialise
 **/
void init_menu( struct menu_item* menu )
{
	/* All of these functions must return a BOOLEAN for consistency with the 
	 * interface. However, in some cases you will need to always return true
	 * (e.g., display items). You should, nevertheless, check the return value
	 * of all the function calls. You will see that a consistent approach makes
	 * the implementation much simpler. */

	int i;

	/* Each of the text options to be displayed when printing the main menu */
	char * menuText [ ] = { 
					"Display Items",
					"Purchase Items", 
					"Save and Exit", 
					"Add Item", 
					"Remove Item", 
					"Display Coins", 
					"Reset Stock", 
					"Reset Coins", 
					"Abort Program" };

	/* Each of the function pointers to be used when called by the menu system*/
	menu_function menuFunctions [] = { 
					display_items,
					purchase_item,
					save_system,
					add_item,
					remove_item,
					display_coins,
					reset_stock,
					reset_coins,
					abort_program };
	
	/* Initialize the Menu structs with the values from above */
	for ( i = 0; i < NUM_MENU_ITEMS; i++ )
	{
		strcpy( menu[ i ].name, menuText[ i ] );
		menu[ i ].function = menuFunctions[ i ]; 
	}
}

/**
 * @return a menu_function that defines how to perform the user's
 * selection
 **/
menu_function get_menu_choice( struct menu_item * menu )
{
	int menuChoice = 0;

	while ( menuChoice == 0 )
	{
		/* I have chosen to simply pass the constant to the ppd_utility.c file
		 * instead of giving it access to the menu.h file ( that contains the
		 * definition of this constant ) for only a SINGLE constant... */
		menuChoice = getValidMenuNumber( NUM_MENU_ITEMS );
	}

	/* We MUST subtract one from the menuChoice as we are now referencing an 
	 * array */
	return menu[ menuChoice - 1 ].function;
}

/* Print each of the menu options to the screen */
void display_menu( struct menu_item * menu )
{
	int i;

	printf( "\n---=== Main Menu ===--- \n");

	for ( i = 0; i < NUM_MENU_ITEMS; i++ )
	{
		if ( i == 3 )
		{
			printf("Administrator-Only Menu \n");
		}
		/* We will need to increment the i value when printing the menu choices
		 * as the menu options range from 1-9, while the array is based on 1-8 */
		printf( "%d.%s \n", i + 1, menu[ i ].name );
	}
}