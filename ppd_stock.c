/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* Gives access to coin.h and shared.h */
#include "ppd_stock.h"

 /**
  * @file ppd_stock.c this is the file where you will implement the 
  * interface functions for managing the stock list.
  **/


/*  ### Adapted from Matthew Bolger's Tutorial Code  ### */
/* Initialize the memory for the stock linked list then return it to the 
 * calling function */
struct ppd_list * stock_init( )
{
	/* Allocate the memory needed for this struct */
	struct ppd_list * item_list = malloc( sizeof( *item_list ) );

	/* Initialize some values into the new struct if the memory was allocated
	 * correctly */
	if( item_list != NULL )
	{
		item_list->head = NULL;
		item_list->count = 0;
	}

	return item_list;
}

/*  ### Adapted from Matthew Bolger's WEEK 8 Tutorial Code  ### */
/* NOTE to simplfiy the code here, item_list is passed instead of passing 
 * the system struct, and then referencing system->item_List for every call */
BOOLEAN addToStockList( struct ppd_list * item_list, 
							struct ppd_stock * stockData )
{
	/* Create a previous and currentNode variables to allow easier access to 
	 * the list */
	struct ppd_node * previousNode, * currentNode;

	/* Create the memory for the new Node to add to the linked list */
	struct ppd_node * newNode = malloc( sizeof( *newNode ) );

	/* If the newNode memory allocation failed */
	if ( newNode == NULL )
	{
		return FALSE;
	}

	/* Assign the stockData that was passed into the function */
	newNode->data = stockData;
	newNode->next = NULL;

	/* Update the node accessing variables */
	previousNode = NULL;
	currentNode = item_list->head;

	/* If the list is empty, we DONT need to compare names hence return */
	if ( item_list->head == NULL )
	{
		item_list->head = newNode;

		/* Increment the item_list node counter */
		item_list->count++;
		return TRUE;
	}

	/* Search through the item_list for when the newNodes name is less than the
	 * next node and place it, thereby sorting the list into alphabetical
	 * order */
	while( currentNode != NULL )
	{
		/* If the newNode's name should come before the currentNode's name in 
		 * alphabetical order */
		if ( strcmp( newNode->data->name, currentNode->data->name ) < 0 )
		{
			break;
		}

		/* Move to the next node */
		previousNode = currentNode;
		currentNode = currentNode->next;
	}

	/* If we are inserting at the head of the list */
	if ( previousNode == NULL )
	{
		newNode->next = item_list->head;
		item_list->head = newNode;
	}
	/* If we are inserting the new node in the middle of the list */
	else
	{
		previousNode->next = newNode;
		newNode->next = currentNode;
	}

	/* Increment the item_list node counter */
	item_list->count++;
	
	return TRUE;
}

/*  ### Adapted from Matthew Bolger's WEEK 8 Tutorial Code  ### */
/* NOTE to simplfiy the code here, item_list is passed instead of passing 
 * the system struct, and then referencing system->item_List for every call */
void removeFromStockList( struct ppd_list * item_list, char targetID[ ] )
{
	/* Create a previous and currentNode variables to allow easier access to
	 * the list */
	struct ppd_node * previousNode, * currentNode;

	/* Update the node accessing variables */
	previousNode = NULL;
	currentNode = item_list->head;

	/* Find the item within the linked list */
	if ( findItem( item_list, targetID, &currentNode, &previousNode ) == FALSE)
	{
		printf( "targetID : %s  WAS NOT FOUND ", targetID );
		return;
	}

	/* Deleting from the head */
	if ( previousNode == NULL )
	{
		item_list->head = currentNode->next;
	}
	else
	{
		previousNode->next = currentNode->next;
	}

	/* Decrement the item_list node counter */
	item_list->count--;

	printf(" \n %s - %s has been removed from the system \n", 
				currentNode->data->id, currentNode->data->name );

	/* Free the deleted Node and its data */
	free( currentNode->data );
	free( currentNode );
}


/* Finds an item within the linked stock list and returns its location through
 * the use of the current and previous nodes */
BOOLEAN findItem( struct ppd_list * item_list, char targetID[ ], 
			 struct ppd_node ** currentNode, struct ppd_node ** previousNode )
{
	/* Search through the item_list for when the currentNodes name is equal to
	 * the targetID and delete it by changing the appropriate pointers  */
	while( *currentNode != NULL )
	{
		if ( strcmp( targetID, (*currentNode)->data->id ) == 0 )
		{
			/* Once the item has been found */
			break;
		}

		/* Move to the next node */
		*previousNode = *currentNode;
		*currentNode = (*currentNode)->next;
	}

	if ( *currentNode == NULL )
	{
		printf(" The target Item ID %s was NOT FOUND \n", targetID );
		return FALSE;
	}

	return TRUE;
}