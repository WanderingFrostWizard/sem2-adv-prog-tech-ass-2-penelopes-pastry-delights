########################################################################
# COSC1076 - Advanced Programming Techniques
# Semester 2 2016 Assignment #2
# Full Name        : Cameron Watt
# Student Number   : s3589163
# Course Code      : BP215
# Program Code     : COSC1076
# Start up code provided by Paul Miller
########################################################################

SOURCES=ppd_main.c ppd_menu.c ppd_options.c ppd_utility.c ppd_stock.c \
ppd_coin.c
HEADERS=ppd_main.h ppd_menu.h ppd_options.h ppd_utility.h ppd_stock.h \
ppd_coin.h ppd_shared.h
OUTPUT=ppd_main.o ppd_menu.o ppd_options.o ppd_utility.o ppd_stock.o \
ppd_coin.o
README=ppd_readme
MAKEFILE=Makefile
REPORTS=requirement13.txt
PROGRAM=ppd
FLAGS=-ansi -pedantic -Wall -g

# PLEASE SEE THE ppd_readme.txt for further information on this diagram
DIAGRAM=PPD_INCLUDES_Diagram.png

########################################################################
# Move this target to the end of the Makefile to zip up your code 
# when submitting. Do not submit your .dat files, or directories. 
# We only want the files that are part of your implementation.
########################################################################

#####
# Addapted from Matthew Bolgers code in tutorial 9
#####

# If the .o files change then re-compile the program
$(PROGRAM): $(OUTPUT)
	gcc $(FLAGS) $(OUTPUT) -o $(PROGRAM)

# If the sourcefile or ANY of the headers change
ppd_main.o: ppd_main.c $(HEADERS)
	gcc $(FLAGS) -c ppd_main.c

ppd_menu.o: ppd_menu.c $(HEADERS)
	gcc $(FLAGS) -c ppd_menu.c

ppd_options.o: ppd_options.c $(HEADERS)
	gcc $(FLAGS) -c ppd_options.c

ppd_utility.o: ppd_utility.c $(HEADERS)
	gcc $(FLAGS) -c ppd_utility.c

ppd_stock.o: ppd_stock.c $(HEADERS)
	gcc $(FLAGS) -c ppd_stock.c

ppd_coin.o: ppd_coin.c $(HEADERS)
	gcc $(FLAGS) -c ppd_coin.c

all: $(PROGRAM)
	gcc $(FLAGS) -c $(SOURCES)
	gcc -ansi -pedantic -Wall $(OUTPUT) -o $(PROGRAM)

clean:
	rm *.o ppd

archive:
	zip s3589163-a2 $(SOURCES) $(HEADERS) $(README) $(REPORTS) $(MAKEFILE) $(DIAGRAM)
