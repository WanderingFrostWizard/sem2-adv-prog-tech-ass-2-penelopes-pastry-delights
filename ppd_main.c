/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* menu.h gives indirect access to ALL of the header files in the program */
#include "ppd_menu.h"

/**
 * @file ppd_main.c contains the main function implementation and any 
 * helper functions for main such as a display_usage() function.
 **/

/**
 * manages the running of the program, initialises data structures, loads
 * data and handles the processing of options. The bulk of this function
 * should simply be calling other functions to get the job done.
 **/
int main( int argc, char **argv )
{
	/* represents the data structures used to manage the system */
	struct ppd_system * system = malloc( sizeof( *system ) );

	/* Value returned from any menu function called */
	BOOLEAN boolReturned = FALSE;

	/* Main Menu data structure */
	struct menu_item menu[ NUM_MENU_ITEMS ];

	/* Temporarily stores the function pointer to the users selected option */
	menu_function selected_Function;

	/* Used to control the menu / program loop */
	BOOLEAN ErrorDetected = FALSE;

	/* init the system */
	if ( system_init( system ) == FALSE )
	{
		printf( "FAILED to initialize the system structs \n" );
		return EXIT_FAILURE;
	}


	printf(" \n ");
	/* validate command line arguments */
	/* Check to see if the user inputs the stock file as an argument */
	if ( argc > 3 ) 
	{
		printf( " This program will accept ONLY TWO arguments \n" );
		return EXIT_FAILURE;
	}
	/* If the user passes in both a stock file and a coins file */
	else if ( argc == 3 )
	{
		system->coin_from_file = TRUE;
	}
	/* If the user passes in just a stock file */
	else if ( argc == 2 )
	{
		printf( " Coin File NOT DETECTED - Loading program with INBUILT coin "
						"structure \n" );
		system->coin_from_file = FALSE;
	}
	else if ( argc < 2 )
	{
		printf( " Please pass in the file paths for the stock file then the "
						"coins file \n" );
		return EXIT_FAILURE;
	}


	/* load data */
	/* As the argv[0] is the filename of ppd.exe, argv[1] will be the stock.dat
	 * and argv[2] will be the coins.dat */
	if ( load_stock( system, argv[1] ) == FALSE )
	{
		printf( " FAILED to load StockFile from: %s \n", argv[1] );
		return EXIT_FAILURE;
	}

	/* If the coin File was not detected or the file fails to load, the coins
	 * array will be initialized to 0 by the system_init() function */
	if ( system->coin_from_file == TRUE )
	{
		if ( load_coins( system, argv[2] ) == FALSE )
		{
			system->coin_from_file = FALSE;
		}
		else 
		{
			printf( " Coins SUCCESSFULLY LOADED from coin file  \n" );
		}
	}

	/* initialise the menu system */
	init_menu( menu );

	/* test if everything has been initialised correctly */


	/* loop, asking for options from the menu */
	while ( ErrorDetected == FALSE )
	{
		display_menu( menu );

		/* Ask the user to select from the previously displayed menu */
		selected_Function = get_menu_choice( menu );

		/* run each option selected */
		boolReturned = selected_Function( system );

		if ( boolReturned == FALSE )
		{
			ErrorDetected = TRUE;
		}		
	}

	/* make sure you always free all memory and close all files 
	 * before you exit the program
	 */
	system_free( system );

	return EXIT_SUCCESS;
}