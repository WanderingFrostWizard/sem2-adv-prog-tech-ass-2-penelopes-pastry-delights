/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* Gives access to coin.h and shared.h */
#include "ppd_stock.h"

/**
 * @file ppd_coin.c implement functions here for managing coins in the
 * "cash register" contained in the @ref ppd_system struct.
**/

/* Convert the integer value received to a price struct */
void centsToPrice( int centsOwed, struct price * convertedPrice )
{   
	convertedPrice->dollars = centsOwed / 100;
	convertedPrice->cents = centsOwed % 100;
}

/* Convert the enum received to an integer value */
int denomToCents( enum denomination coin_name )
{
	switch( coin_name )
	{
		case FIVE_CENTS:
		{
			return 5;
		}
		case TEN_CENTS:
		{
			return 10;
		}
		case TWENTY_CENTS:
		{
			return 20;
		}
		case FIFTY_CENTS:
		{
			return 50;
		}
		case ONE_DOLLAR:
		{
			return 100;
		}
		case TWO_DOLLARS:
		{
			return 200;
		}
		case FIVE_DOLLARS:
		{
			return 500;
		}
		case TEN_DOLLARS:
		{
			return 1000;
		}
		default:
		{
			return -1;
		}
	}
}

/* Convert the integer value received to a denomination enum */
enum denomination centsToDenom( int * cents )                
{
	switch( *cents )
	{
		case 5:
		{
			return FIVE_CENTS;
		}
		case 10:
		{
			return TEN_CENTS;
		}
		case 20:
		{
			return TWENTY_CENTS;
		}
		case 50:
		{
			return FIFTY_CENTS;
		}
		case 100:
		{
			return ONE_DOLLAR;
		}
		case 200:
		{
			return TWO_DOLLARS;
		}
		case 500:
		{
			return FIVE_DOLLARS;
		}
		case 1000:
		{
			return TEN_DOLLARS;
		}
		  default:
		  {
				*cents = 0;
				return FIVE_CENTS;
		  }
	}
}

/* Return the name of the denomination as a string */
char* denomToText( enum denomination coin_name )
{
	switch( coin_name )
	{
		case FIVE_CENTS:
		{
			return " 5 Cents";
		}
		case TEN_CENTS:
		{
			return "10 Cents";
		}
		case TWENTY_CENTS:
		{
			return "20 Cents";
		}
		case FIFTY_CENTS:
		{
			return "50 Cents";
		}
		case ONE_DOLLAR:
		{
			return " 1 Dollar";
		}
		case TWO_DOLLARS:
		{
			return " 2 Dollar";
		}
		case FIVE_DOLLARS:
		{
			return " 5 Dollar";
		}
		case TEN_DOLLARS:
		{
			return "10 Dollar";
		}
		  /* This can never happen as either a new enumeration was introduced and
			* the programmer didn't check, or a value that is not within the 
			* enumeration values is entered, which would cause a SEG FAULT
			* LONG before this statement is EVER REACHED */
		default:
		{
			return " ";
		}
	}
}

/* Checks to see if the coin is valid. Also increments the cash_Register
 * array. */
BOOLEAN isValidCentsValue ( int centsEntered, struct ppd_system * system )
{
	switch( centsEntered )
	{
		case 5: 
		{
			system->cash_register[ FIVE_CENTS ].count++;
				return TRUE;
		}
		case 10:
		{
			system->cash_register[ TEN_CENTS ].count++;
				return TRUE;
		}
		case 20:
		{
			system->cash_register[ TWENTY_CENTS ].count++;
				return TRUE;
		}
		case 50:
		{
			system->cash_register[ FIFTY_CENTS ].count++;
				return TRUE;
		}
		case 100:
		{
			system->cash_register[ ONE_DOLLAR ].count++;
				return TRUE;
		}
		case 200:
		{
			system->cash_register[ TWO_DOLLARS ].count++;
				return TRUE;
		}
		case 500:
		{
			system->cash_register[ FIVE_DOLLARS ].count++;
				return TRUE;
		}
		case 1000:
		{
			system->cash_register[ TEN_DOLLARS ].count++;
			/* This will return TRUE if centsEntered is any value above as
			 * there are no break statements, control will "fall" through the
			 * switch case and finish here */
			return TRUE;
		}
		default:
		{
			return FALSE;
		}
	}
}