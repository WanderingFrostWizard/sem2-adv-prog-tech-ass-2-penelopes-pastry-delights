/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* For coin struct definition, and indirect access to shared.h */
#include "ppd_coin.h"         

#ifndef PPD_STOCK
#define PPD_STOCK

/**
 * @file ppd_stock.h this file defines the data structures required to 
 * manage the stock list. You should add here the function prototypes for
 * managing the list here and implement them in ppd_stock.c
 **/

/**
 * The length of the id string not counting the null terminator
 **/
#define IDLEN 5

/**
 * The maximum length of a product name not counting the null terminator
 **/
#define NAMELEN 40

/**
 * The maximum length of a product description not counting the null
 * terminator.
 **/
#define DESCLEN 255

/* The maximum length of a price, read from the coins file, not counting the
 * null terminator is XX.XX  ie 5 characters, including the delimiter */
#define PRICELEN 5

/* The maximum length of an on_hand quantity is 2, as the highest on_hand 
 * quantity we are expecting is 99 */
#define ONHANDLEN 2

/* The number of STOCK_DELIMITER 's we are expecting in the stockFile */
#define NUMDELIMITERS 4

/**
 * The default coin level to reset the coins to on request
 **/
#define DEFAULT_COIN_COUNT 20

/**
 * The default stock level that all new stock should start at and that 
 * we should reset to on restock
 **/
#define DEFAULT_STOCK_LEVEL 20

/**
 * The number of denominations of currency available in the system 
 **/
#define NUM_DENOMS 8

/* The delimiter between fields within the stock file */
#define STOCK_DELIMITER "|"

/**
 * a structure to represent a price. One of the problems with the floating
 * point formats in C like float and double is that they have minor issues
 * of inaccuracy due to rounding. In the case of currency this really is
 * not acceptable so we introduce our own type to keep track of currency.
 **/
struct price
{
	/**
	 * the dollar value for some price
	 **/
	unsigned dollars, 
			 /**
			  * the cents value for some price
			  **/
			 cents;
};

/**
 * data structure to represent a stock item within the system
 **/
struct ppd_stock
{
	/**
	 * the unique id for this item
	 **/
	char id[ IDLEN + 1 ];
	/**
	 * the name of this item
	 **/
	char name[ NAMELEN + 1 ];
	/**
	 * the description of this item
	 **/
	char desc[ DESCLEN + 1 ];
	/**
	 * the price of this item
	 **/
	struct price price;
	/**
	 * how many of this item do we have on hand? 
	 **/
	unsigned on_hand;
	/**
	 * a pointer to the next node in the list           ##### UN-NECCESSARY COMMENT WITHIN STARTUP CODE #####
	 **/                                   
};

/**
 * the node that holds the data about an item stored in memory
 **/
struct ppd_node
{
	/* pointer to the data held for the node */
	struct ppd_stock * data;
	/* pointer to the next node in the list */
	struct ppd_node * next;
};

/**
 * the list of products - each link in the list is a @ref ppd_node
 **/
struct ppd_list  
{
	/**
	 * the beginning of the list
	 **/
	struct ppd_node * head;
	/**
	 * how many nodes are there in the list?
	 **/
	unsigned count;
};

/**
 * this is the header structure for all the datatypes that are 
 * passed around and manipulated
 **/
struct ppd_system
{
	/**
	 * the container for all the money manipulated by the system
	 **/
	struct coin cash_register[ NUM_DENOMS ];

	/**
	 * the linked list - NOTE that this is a pointer - how does that
	 * change what we need to do for initialization of the list?
	 **/
	struct ppd_list * item_list;

	/**
	 * the name of the coin file - we need this for saving as all menu
	 * items only take the one parameter of the ppd_system
	 **/
	const char * coin_file_name;
	/**
	 * the name of the stock file
	 **/
	const char * stock_file_name;

	/* are the coins loaded in from a file ? */
	BOOLEAN coin_from_file;
};

/* Initializes the main node of the linked list and returns it to the calling
 * function */
struct ppd_list * stock_init( );

/* Add an item from the linked list */
BOOLEAN addToStockList( struct ppd_list * item_list, struct ppd_stock * data );

/* Remove an item from the linked list */
void removeFromStockList( struct ppd_list * item_list, char targetID[ ] );

/* Finds an item within the linked stock list and returns its location through
 * the use of the current and previous nodes */
BOOLEAN findItem( struct ppd_list * item_list, char targetID[ ], 
			struct ppd_node ** currentNode, struct ppd_node ** previousNode );

#endif