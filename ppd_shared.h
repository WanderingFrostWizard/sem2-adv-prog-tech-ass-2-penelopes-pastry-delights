/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* Anything within this file can be accessed by ANY other file within this 
 * program. With this new layout, this header file would have been the IDEAL 
 * location to declare ALL CONSTANTS and EVERY MAJOR structure within the 
 * program eg ppd_system, denominations etc */
#include <stdio.h>
#include <stdlib.h>         
#include <string.h>   

#ifndef PPD_SHARED
#define PPD_SHARED

#define EXTRACHARS 2 

/**
 * datatype to represent a boolean value within the system
 **/
typedef enum truefalse
{
	/**
	 * the constant for false
	 **/
	FALSE, 
	/**
	 * the constant for true
	 **/
	TRUE
} BOOLEAN;

#endif
