 /***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* Also gives access to main.h, stock.h, coin.h and shared.h */
#include "ppd_utility.h"

/**
 * @file ppd_utility.c contains implementations of important functions for
 * the system. If you are not sure where to implement a new function, 
 * here is probably a good spot.
 **/

void read_rest_of_line( void )
{
	int ch;
	/* keep retrieving characters from stdin until we
	 * are at the end of the buffer
	 */
	while( ch = getc( stdin ), ch!='\n' && ch != EOF )
		;
	/* reset error flags on stdin */
	clearerr( stdin );
}

/**
 * @param system a pointer to a @ref ppd_system struct that holds all 
 * the data for the system we are creating
 **/
BOOLEAN system_init( struct ppd_system * system )
{
	int i;

	/* Initialize the denomination values in their positions based upon the
	 * denominations enum in coin.h */
	system->cash_register[ FIVE_CENTS ].denom = FIVE_CENTS;
	system->cash_register[ TEN_CENTS ].denom = TEN_CENTS;
	system->cash_register[ TWENTY_CENTS ].denom = TWENTY_CENTS;
	system->cash_register[ FIFTY_CENTS ].denom = FIFTY_CENTS;
	system->cash_register[ ONE_DOLLAR ].denom = ONE_DOLLAR;
	system->cash_register[ TWO_DOLLARS ].denom = TWO_DOLLARS;
	system->cash_register[ FIVE_DOLLARS ].denom = FIVE_DOLLARS;
	system->cash_register[ TEN_DOLLARS ].denom = TEN_DOLLARS;

	/* Initialize the cash_register to NULL values */
	for ( i = 0; i < NUM_DENOMS; i++ )
	{
		system->cash_register[ i ].count = 0;
	}

	/* Initialize the stock linked list and assign to the system struct */
	system->item_list = stock_init( );

	/* Initialize the coin_file_name */
	system->coin_file_name = NULL;   

	/* Initialize the stock_file_name */
	system->stock_file_name = NULL;   

	/* Initialize the coin_from_file variable */
	system->coin_from_file = FALSE;

	return TRUE;
}

/**
 * loads the stock file's data into the system. This needs to be 
 * implemented as part of requirement 2 of the assignment specification.
 **/
 /* ### Code adapted from tutorial code by Matthew Bolger ### */
BOOLEAN load_stock( struct ppd_system * system, const char * filename )
{
	/* Declare a File variable to contain the contents of the stockFile when 
	 * loaded into the system  */
	FILE *stockFile;

	/* Create a buffer to handle the file transfers */
	char buffer[ IDLEN + NAMELEN + DESCLEN + PRICELEN + ONHANDLEN + NUMDELIMITERS + 1 ];

	/* Open the file ready for reading */
	stockFile = fopen( filename, "r" );
	if ( stockFile == NULL )
	{
		printf("\n Failed to open the Stock file : %s \n", filename );
		return FALSE;
	}

	/* Assign the filename of the stock file variable within the system 
	 * struct */
	system->stock_file_name = filename;
	
	while ( fgets ( buffer, sizeof( buffer ), stockFile ) != NULL )
	{
		
		/* Create a data node to store the data received from the stock File */
		struct ppd_stock * stockData = malloc( sizeof( *stockData ) );
		char * token;

		token = strtok( buffer , STOCK_DELIMITER );
		if ( token == NULL )
		{
			/* Reached end of File */
			free( stockData );
			break;
		}
		strcpy( stockData->id, token ); 

		token = strtok( NULL , STOCK_DELIMITER );
		if ( token == NULL )
		{
			printf(" INVALID token detected \n");
			fclose( stockFile );
			return FALSE;
		}
		strcpy( stockData->name, token ); 

		token = strtok( NULL , STOCK_DELIMITER );
		if ( token == NULL )
		{
			printf(" INVALID token detected \n");
			fclose( stockFile );
			return FALSE;
		}
		strcpy( stockData->desc, token );

		token = strtok( NULL , "." );
		if ( token == NULL )
		{
			printf(" INVALID token detected \n");
			fclose( stockFile );
			return FALSE;
		}
		stockData->price.dollars = strtol( token, NULL, 10 );

		token = strtok( NULL , STOCK_DELIMITER );
		if ( token == NULL )
		{
			printf(" INVALID token detected \n");
			fclose( stockFile );
			return FALSE;
		}
		stockData->price.cents = strtol( token, NULL, 10 );        

		token = strtok( NULL , "\n" );
		if ( token == NULL )
		{
			printf(" INVALID token detected \n");
			fclose( stockFile );
			return FALSE;
		}
		stockData->on_hand = strtol( token, NULL, 10 );

		/* Transfer the data struct to the system->item_list->data struct 
		 * within the system struct */
		addToStockList( system->item_list, stockData );
	}

	fclose( stockFile );

	/* Once the file has been successfully loaded into the system struct and
	 * the file has been closed, return TRUE */
	return TRUE;
}

BOOLEAN save_Stock( struct ppd_system * system )
{
	/* Declare a variable to use as the stock file input stream */
	FILE *stockFile;

	struct ppd_node * currentNode;

	const char * filename = system->stock_file_name;

	/* Open the filen with write permissions */
	stockFile = fopen( filename, "w" );
	if ( stockFile == NULL )
	{
		return FALSE;
	}
	/* Set the current node to the start of the list */
	currentNode = system->item_list->head;

	while ( currentNode != NULL )
	{
		/* Output each of the stock items details to file and add the stock 
		 * field delimiter */
		fprintf( stockFile, "%s%s", currentNode->data->id, STOCK_DELIMITER );
		fprintf( stockFile, "%s%s", currentNode->data->name, STOCK_DELIMITER );
		fprintf( stockFile, "%s%s", currentNode->data->desc, STOCK_DELIMITER );

		/* Output price to file with the cents formatted to show 2 characters */
		fprintf( stockFile, "%u.%02u%s", currentNode->data->price.dollars, 
							currentNode->data->price.cents, STOCK_DELIMITER );
		fprintf( stockFile, "%u", currentNode->data->on_hand );

		/* Output NEWLINE */
		fprintf( stockFile, "\n" );

		/* Move to the next node */
		currentNode = currentNode->next;
	}

	/* Close File */
	fclose( stockFile );

	printf(" Stock Successfully saved to %s \n", filename );

	return TRUE;
}

/**
 * loads the contents of the coins file into the system. This needs to
 * be implemented as part 1 of requirement 18.
 **/
BOOLEAN load_coins( struct ppd_system * system, const char * filename )
{
	/* Declare a File variable to contain the contents of the coinFile when
	 * loaded into the system  */
	FILE *coinFile;

	/* Create a buffer to handle the file transfers */
	/* The buffer will need to accept:
	 * 4 characters for the largest cents value,
	 * 1 character for the COIN_DELIM and
	 * 2 characters for the maximum on_hand
	 * We can use the same ONHANDLEN from stock, as the maximum number of coins
	 * is also 99 */
	char buffer[ CENTSLEN + 1 + ONHANDLEN + 1 ];

	/* Open the coinFile ready for reading */
	coinFile = fopen( filename, "r" );
	if ( coinFile == NULL )
	{
		printf("Failed to open the Coin file : %s\n", filename);
		return FALSE;
	}

	/* Assign the filename of the coin file variable within the system struct*/
	system->coin_file_name = filename;
	
	while ( fgets ( buffer, sizeof( buffer ), coinFile ) != NULL )
	{
		/* Create a temporary struct to store the current coin received from
		 * the coins File */
		struct coin currentDenom;
		char * token;
		int coin_Value = 0;

		token = strtok( buffer , COIN_DELIM );

		coin_Value = strtol( token, NULL, 10 );

		if ( token == NULL || coin_Value == 0 )
		{
			break;
		}

		currentDenom.denom = centsToDenom( &coin_Value );

			/* If coin_Value is invalid, centsToDenom will return this */
			if ( currentDenom.denom == FIVE_CENTS && coin_Value == 0 )
			{
				printf(" INVALID denomination detected \n");
				fclose( coinFile );
				return FALSE;
			}

		token = strtok( NULL , "\n" );
		if ( token == NULL )
		{
			printf(" INVALID token detected \n");
			fclose( coinFile );
			return FALSE;
		}
		currentDenom.count = strtol( token, NULL, 10 );

		/* Assign the current denomination to its relevant position in the 
		 * cash_register Array. The array will be sorted in the same order as 
		 * the enum denominationm regardless of the order the denominations are
		 * loaded from file */
		system->cash_register[ currentDenom.denom ] = currentDenom;
	}

	fclose( coinFile );

	/* Once the file has been successfully loaded into the system struct and
	 * the file has been closed, return TRUE */
	return TRUE;
}

BOOLEAN save_Coins( struct ppd_system * system )
{
	/* Declare a variable to use as the coin file input stream */
	FILE *coinFile;

	int i;

	const char * filename = system->coin_file_name;

	/* Open the filen with write permissions */
	coinFile = fopen( filename, "w" );
	if ( coinFile == NULL )
	{
		return FALSE;
	}

	for ( i = 0; i < NUM_DENOMS; i++ )
	{
		/* Convert denomination to integer value */
		fprintf( coinFile, "%d%s%d", 
					denomToCents( system->cash_register[ i ].denom ),
					COIN_DELIM, system->cash_register[ i ].count ); 

		/* Output NEWLINE */
		fprintf( coinFile, "\n" );
	}

	/* Close File */
	fclose( coinFile );

	printf(" Coins Successfully saved to %s \n", filename );

	return TRUE;
}

/**
 * @param system a pointer to a @ref ppd_system struct that holds all 
 * the data for the system we are creating
 **/
void system_free( struct ppd_system * system )
{
	/* Create a previous and currentNode variables to allow easier access to 
	 * the list */
	struct ppd_node * previousNode, * currentNode;

	/* Free every node in the item_list */
	if ( system->item_list->head != NULL )
	{
		currentNode = system->item_list->head;

		/* While not at the end of the list */
		while( currentNode->next != NULL )
		{
			/* Move to the next node and free the previous */
			previousNode = currentNode;
			currentNode = currentNode->next;
			free( previousNode->data );
			free( previousNode );
		}
	}
	free ( currentNode->data );
	free ( currentNode );

	/* Free the item_list */
	free( system->item_list );

	free( system );
}

/* Adapted from my assignment 1 code */
/* Asks the user to enter an integer to select an option from the main menu,
 * it then validates that integer and if successful, returns it to the calling
 * function */
int getValidMenuNumber ( int NUM_MENU_ITEMS )
{
	/* Clear the Input variable if the loop is repeated */
	char input[ 1 + EXTRACHARS ];
	int numberEntered = 0;

	printf ( " Please select an option from above (1-%d) : ", NUM_MENU_ITEMS );
	
	/* Get the user input. NOTE this WILL only allow a SINGLE character, which
	 * will only allow for a MAXIMUM of 9 menu options, this should be changed
	 * to allow any future modifications */
	if ( fgets( input, 1 + EXTRACHARS, stdin ) == NULL || input[ 0 ] == '\n' )
	{
		return 0;
	}
	else if ( input[ 1 ] == '\0' ) 
	{
		printf ( " Not enough characters Detected \n" );
		return 0;
	}
	/* Check to see if the user has entered MORE THAN 1 character */
	else if ( input[ 1 ] != '\n' )    
	{
		printf ( " Excess Input Detected \n" );
		read_rest_of_line();  /* Clear the rest of the buffer */
		return 0;
	}
	else
	{
		numberEntered = strtol( input, NULL, 10 );

		/* If the numberEntered is NOT within the bounds of the Main Menu */
		if ( numberEntered < 1 || numberEntered > NUM_MENU_ITEMS )
		{
			printf ( " Invalid Input Detected \n" );
			return 0;
		}
		else 
		{
			return numberEntered;
		}
	}
}

/* Adapted from my assignment 1 code */
BOOLEAN getValidItemID ( char * input )
{
	printf ( " Please enter the ID of the item you wish to purchase : " );
	
	/* Check to see if fgets FAILS or the user presses enter on a new line */
	if ( fgets( input, IDLEN + EXTRACHARS, stdin ) == NULL ||
																 input[ 0 ] == '\n' )
	{
		printf ( " USER wants to quit " );
		return FALSE;
	}
	/* Check to see if the user has entered less than the IDLEN of characters 
	 * (hence ID will be TOO SHORT ) */
	else if ( input[ IDLEN ] == '\0' ) 
	{
		printf ( " Not enough characters Detected \n" );
		return FALSE;
	}
	/* Check to see if the user has entered MORE than the maximum number of 
	 * characters */
	else if ( input[ IDLEN ] != '\n' )    
	{
		printf ( " Excess Input Detected \n" );
		read_rest_of_line();  /* Clear the rest of the buffer */
		return FALSE;
	}
	/* Check to see if the user has entered the item identifier character */
	else if ( input[ 0 ] != 'I' ) 
	{
		printf ( " Invalid Identifier Detected \n" );
		return FALSE;
	}
	else
	{
		/* Change the newline character stored at the end of the string to be
		 * a '\0' character */
		input[ IDLEN ] = '\0';
		return TRUE;
	}
}


/* Adapted from my assignment 1 code */
/* This function asks the user to enter the amount of money inserted into the
 * machine (in cents). If this function fails it returns 0, if the user wants
 * to quit or due to serious failure, the function retuns -1, else it will 
 * return the number entered by the user. This function is also used to enter
 * a price when adding a new item to the system */
int getCents ( )
{
	int numberEntered; 

	/* Clear the Input variable if the loop is repeated */
	char input[ CENTSLEN + EXTRACHARS ] = " ";

	/* If fgets FAILS or the user presses enter on a new line */
	if ( fgets( input, CENTSLEN + EXTRACHARS, stdin ) == NULL ||
				 input[0] == '\n' )
	{
		return -1;
	}               
	/* If the user has entered upto the CENTSLEN character maximum, the fifth 
	 * character (ie [CENTSLEN]) position value should be == '\n', as this is 
	 * where the user pressed enter. This can also be equal to a '\0' if the 
	 * user has entered less than CENTSLEN characters */
	else if ( input[ CENTSLEN ] == '\n' || input[ CENTSLEN ] == '\0' )
	{
		numberEntered = strtol( input, NULL, 10 );

		/* If the numberEntered is smaller than the smallest denomination or
		 * larger than the largest, then it can be eliminated quickly */
		if ( numberEntered < SMALLEST_DENOM_CENTS ||
						 numberEntered > LARGEST_DENOM_CENTS )
		{
			return 0;
		}
		else            
		{
			return numberEntered;
		}
	}
	else if ( input[ CENTSLEN ] != '\n' )    
	{
		read_rest_of_line();  /* Clear the rest of the buffer */
		return 0;
	}
	/* ERROR STATE, resolved by returning -1 to the calling function and dealing
	 * with it there */
	else 
	{
		printf( "ERROR in getCents \n" );
		return -1;
	}
}


/* Adapted from my assignment 1 code */
/* Generic function used to get any text input eg Item Name, description etc */
BOOLEAN getText ( char * input, int maxLength )
{
	int i;
	
	/* Check to see if fgets FAILS or the user presses enter on a new line */
	if ( fgets( input, maxLength + EXTRACHARS, stdin ) == NULL ||
																	 input[ 0 ] == '\n' )
	{
		return FALSE;
	}
	/* If the user has entered less than or upto the maxLength of characters */
	else if ( input[ maxLength ] == '\0' || input[ maxLength ] == '\n' )   
	{
		/* Find the newline character at the end of the array and change it to
		 * be a '\0' character */
		for ( i = 0; i < maxLength; i++)
		{
			if ( input[ i ] == '\n')
			{
				input[ i ] = '\0';
				break;
			}
		}
		return TRUE;
	}
	/* Check to see if the user has entered MORE than the maximum number of 
	 * characters */
	else if ( input[ maxLength ] != '\n' )    
	{
		printf ( " Excess Input Detected \n" );
		read_rest_of_line();  /* Clear the rest of the buffer */
		return FALSE;
	}
	else 
	{
		printf( "ERROR in getText \n" );
		return FALSE;
	}

}

/* Asks the user to enter a valid price and validates the input until correct,
 * or the user quits */
int getValidPrice ( )
{
	int price;
	BOOLEAN isValid = FALSE;

	while ( isValid == FALSE )
	{
		printf ( " Please enter the cost of the item you wish to Add : " );

		price = getCents( );

		  /* User wants to quit */
		  if ( price == 0 )
		  {
				return -1;
		  }
		  else if ( price == 0 )
		{
			printf( " Invalid Denomination " );
		}
		/* If the value is divisible by the smallest denomination and leaves NO
		 * remainder, then this is a valid price */
		else if ( price % SMALLEST_DENOM_CENTS == 0 )
		{
			isValid = TRUE;
		}
		else if ( price != 0 )
		{
			printf( " Invalid Price \n" );
		}
	}
	return price;
}



/* Used to confirm if there is enough change to return to the customer and also
 * to return that change to the customer by deducting coins from the 
 * cash_Register. If the system can return the amount in change from the till,
 * this function will return true, else it will return false, and any change 
 * that has been counted out to the customer will be returned to the 
 * cash_Register */
BOOLEAN changeCountBack( int changeOwed, struct ppd_system * system )
{
	int coinsGiven[ NUM_DENOMS ];
	/*struct coin currentDenom;*/
	int i;
	int denomValue;

	/* Initialize the array */
	for ( i = 0; i < NUM_DENOMS; i++ )
	{
		coinsGiven[ i ] = 0;    
	}

	/* Checks if their is enough change to return to the customer and stores 
	 * the number of coins needed to achieve this in the coinsGiven array. */
	for ( i = NUM_DENOMS - 1; i > -1; i-- )
	{
		denomValue = denomToCents( system->cash_register[ i ].denom );

		/* Continue the current "for loop", while there is still sufficient 
		 * quantity of the current denomination and the current denomination
		 * is not TOO large for the amount owed */
		while ( system->cash_register[ i ].count > 0 && 
												( changeOwed - denomValue >= 0 ) )
		{
			/* Add this to the coinsGiven */
			coinsGiven[ i ]++;

			/* Decreases the amount of change to return */
			changeOwed -= denomValue;
		}        
	}

	/* If there was insufficient change in the till return FALSE and return to
	 * the calling function NOTE that the cash_Register has NOT been altered */
	if ( changeOwed > 0 )
	{
		return FALSE;
	}
	/* If the correct amount of change was in the till, then return it to the 
	 * customer */
	else if ( changeOwed == 0 )
	{ 
		returnChange( TRUE, system, coinsGiven );
		return TRUE;
	}

	return FALSE;
}   

/* Print out the coinsToReturn one at a time as a single string to simulate 
 * coins being return to the user */
void returnChange( BOOLEAN fromRegister, struct ppd_system * system,
						 int coinsToReturn[] )
{
	int i;

	printf("Change returned: " );
	for ( i = NUM_DENOMS - 1; i > -1; i-- )
	{
		while ( coinsToReturn[ i ] > 0 )
		{
			/* Return the change to the customer */
			printf("%s, ", denomToText( i ) );

			if ( fromRegister == TRUE )
			{
				/* Remove the current coin from the cash_Register */
				system->cash_register[ i ].count--; 
			}

			/* Update the change returned */
			coinsToReturn[ i ]--;  
		}
	}
	printf(" \n");
}

/* Checks through the stock List to find the longest name length and returns it
 * as an integer */
int getLargestNameLength( struct ppd_system * system )
{
	struct ppd_node * currentNode = system->item_list->head;
	int currentNameLength = 0;
	int longestNameLength = 0;

	while( currentNode != NULL )
	{
		currentNameLength = strlen( currentNode->data->name );

		if ( longestNameLength < currentNameLength )
		{
			longestNameLength = currentNameLength;
		}
		/* Check the next Node */
		currentNode = currentNode->next;
	}
	return longestNameLength;
}

/* Find the highest ID number within the item_list, then return this number + 1
 * in the correct format as a string */
char* findNextAvailableID( struct ppd_list * item_list, 
				struct ppd_node ** currentNode, struct ppd_node ** previousNode )
{
	char* ID = "I0000";

	/* If there are no items within the List */
	if ( *currentNode == NULL )
	{
		return "I0001";
	}

	/* Search through the item_list for when the currentNodes name is equal to
	 * the targetID and delete it by changing the appropriate pointers  */
	while( (*currentNode) != NULL )
	{
		if ( strcmp( ID, (*currentNode)->data->id ) < 0 )
		{
			ID = (*currentNode)->data->id;
			/* Once the item has been found */
		}

		/* Move to the next node */
		(*currentNode) = (*currentNode)->next;
	}

	return ID;
}

/* Used to autoGenerate the next avaialable Item ID Number */
void generateItemID( struct ppd_system * system, char newID [] )
{
	char * highestID;	  /* Stores Original ID Number */
	char * IDNo;  /* Stores the integer from ID Number as string */
	int IDNoInt;  /* Stores the converted ID Number as integer */

	/* The nodes are used only to find the next ID number */
	struct ppd_node * previousNode;
	struct ppd_node * currentNode = system->item_list->head;
	
	/* Find the next available ID Number within the stock list */
	highestID = findNextAvailableID( system->item_list, &currentNode,
																			 &previousNode );

	/* As highestID is a reference to the ID number of an item stored within
	 * the item_List, we need to make a copy of the Item ID to avoid changes
	 * to the original ID stored within the item_List */
	strcpy( newID, highestID );

	/* Remove the Item Identifier */
	strtok( newID , "0" );

	/* Store the integer portion of the Item ID */
	IDNo = strtok( NULL , "\n" );

	/* Convert the Concatonated ID Number to an integer */
	IDNoInt = strtol( IDNo, NULL, 10 );

	/* Store the next Item ID +1 larger than the ID found above */
	sprintf( newID, "I%04d", IDNoInt + 1 );
}