/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* gives access to every header file except menu.h */
#include "ppd_options.h"

/**
 * @file ppd_options.c this is where you need to implement the main 
 * options for your program. You may however have the actual work done
 * in functions defined elsewhere. 
 * @note if there is an error you should handle it within the function
 * and not simply return FALSE unless it is a fatal error for the 
 * task at hand. You want people to use your software, afterall, and
 * badly behaving software doesn't get used.
 **/

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this particular function should never fail.
 **/
BOOLEAN display_items( struct ppd_system * system )
{
	struct ppd_node * currentNode = system->item_list->head;

	/* Find the length of the largest name, and set the column width with it */
	int largestNameLength = getLargestNameLength( system );

	printf ( "\n \t \t \t---=== Items Menu ===--- \n" );
	printf ( "%-*s | %-*s | %-*s | %s\n", IDLEN, "ID", largestNameLength, 
													"Name", 9 ,"Available", "Price" );
	printf ( "----------------------------------------------------------------"
													"------------- \n" );

	/* Display the formatted output from each Node->data */
	while( currentNode != NULL )
	{
		printf ( "%-*s |", IDLEN, currentNode->data->id );
		printf ( " %-*s |", largestNameLength, currentNode->data->name );
		printf ( " %-*d |", 9, currentNode->data->on_hand );
		printf ( " $ %d.%02d \n", currentNode->data->price.dollars, 
									currentNode->data->price.cents );

		currentNode = currentNode->next;
	}
	printf(" Stock items : %u \n", system->item_list->count );

	/* As it is impossible for this function to fail */
	return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a purchase succeeds and false when it does not
 **/
BOOLEAN purchase_item( struct ppd_system * system )
{
	/* Used to initialize the coinsInserted Array */
	int i;

	/* Used to get and store the item_ID to be compared */
	char input [ IDLEN + EXTRACHARS ] = " ";
	BOOLEAN boolReturned = FALSE;
	BOOLEAN isValidDenomination = FALSE;

	int itemPrice = 0;
	int centsOwed = 0;
	int centsEntered = 0;
	int coinsInserted [ NUM_DENOMS ];

	/* Used to store the money owed to the vending machine, and once the sale
	 * is complete, it is re-used to represent the money owed to the customer*/
	struct price * moneyOwing = malloc( sizeof( *moneyOwing ) ); 

	/* Used to store a pointer to the item selected by the user, to allow easy 
	 * editing */
	struct ppd_stock * item_Selected;

	struct ppd_node * currentNode;
	currentNode = system->item_list->head;

	/* Initialize the coinsInserted Array */
	for ( i = 0; i < NUM_DENOMS; i++ )
	{
		coinsInserted[ i ] = 0;    
	}

	/* Display Product Menu */
	display_items( system );

	/* Ask customer for selection */
	boolReturned = getValidItemID ( input );
	if ( boolReturned == FALSE )
	{
		/* Invalid input or user wants to quit */
		return TRUE;
	}

	/* Check the ID entered against the items stored */
	while( currentNode != NULL )
	{
		if ( strcmp( currentNode->data->id, input ) == 0 )
		{
			/* Store a reference to the selected item */
			item_Selected = currentNode->data;

			if ( item_Selected->on_hand <= 0 ) 
			{
				printf("Sorry we are out of %s's \n", item_Selected->name);
				free( moneyOwing );
				return TRUE;
			}
			break;
		}
		/* Move to the next node and continue checking */
		currentNode = currentNode->next;
	}

	printf("You have selected \" %s - %s \" which costs $%u.%02u \n", 
					item_Selected->name, item_Selected->desc, 
					item_Selected->price.dollars, item_Selected->price.cents );

	/* Ask customer to input money */
	printf("Please insert coins - type in the value of each note/coin "
											"in CENTS \n" );
	printf("Press Enter on a empty line to cancel this purchase \n" );

	itemPrice = ( item_Selected->price.dollars * 100 ) + 
									item_Selected->price.cents;
	centsOwed = itemPrice;

	do 
	{
		/* Reset these Loop variables from any previous loops */
		centsEntered = 0;
		isValidDenomination = FALSE;

		/* Re-Calculate the moneyOwing from the centsOwed */
		centsToPrice( centsOwed, moneyOwing );
		
		/* Ask the customer for a valid denomination until a valid int is
		 * received or the user quits */
		while ( isValidDenomination == FALSE )
		{
			printf("There is still $%u.%02u owing : ", moneyOwing->dollars, 
											moneyOwing->cents );

			centsEntered = getCents ( );

			/* If the user wants to quit, we must return any money they have
			 * inserted */
			if ( centsEntered == -1 )
			{
				/* Calculate the money the user has inserted */
				centsToPrice( ( itemPrice - centsOwed ), moneyOwing );

				printf(" You have selected to quit. Here is your change of"
								" $ %u.%02u Please Come again. \n", 
								moneyOwing->dollars, moneyOwing->cents ); 
				returnChange( FALSE, system, coinsInserted ); 

				/* Free the temporary variables before returning to the 
				 * previous function */
				free( moneyOwing );
				return TRUE;
			}
			/* If the centsEntered is still currently valid, we can pass it
			 * to get checked against the available denominations */

			isValidDenomination = isValidCentsValue ( centsEntered, system );
			if ( centsEntered == 0 || isValidDenomination == FALSE )
			{
				printf( " Invalid Denomination " );
			}
		}

		/* Update the coinsInserted Array */
		coinsInserted[ centsToDenom( &centsEntered ) ]++;

		/* Reduce the cents owed by the money inserted */
		centsOwed -= centsEntered;
	} while ( centsOwed > 0 );

	/* If there is no change owing to the customer */
	if ( centsOwed == 0 )
	{
		centsToPrice( 0, moneyOwing );
	}   
	/* If there is change to be returned to the customer */
	else 
	{
		/* Update the moneyOwing with the Change that is OWED to the customer.  
		 * This is achieved by using the negative of centsOwed, as we already
		 * know that centsOwed MUST be < 0, to enter this function, hence this
		 * variable is now showing how much change we need to return to the 
		 * customer */
		centsToPrice( -centsOwed, moneyOwing );

		/* If we are unable to return the correct change due to lack of change 
		 * in the cash_Register */
		if ( changeCountBack( -centsOwed, system ) == FALSE )
		{
			/* ERROR MESSAGE ALREADY DISPLAYED WITHIN THE FUNCTION */
			printf("ERROR insufficient change in this machine to return "
							"$ %u.%02u \n", moneyOwing->dollars, moneyOwing->cents );
			printf("Here is your inserted currency: " );
			returnChange( FALSE, system, coinsInserted );  

			free( moneyOwing );

			return TRUE;          
		}
	} 

	printf("Thanks for your purchase. Here is your %s, and your change of"
					" $ %u.%02u \n", item_Selected->name, 
					moneyOwing->dollars, moneyOwing->cents );
	printf("Please come back soon \n");

	/* Decrease the stock on hand for the item purchased */
	item_Selected->on_hand -= 1;

	free( moneyOwing );

	return TRUE;
}


/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a save succeeds and false when it does not
 **/
BOOLEAN save_system( struct ppd_system * system )
{
	if ( save_Stock( system ) == FALSE )
	{
		printf("\n Failed to save to the Stock file : %s \n", 
						system->stock_file_name );
		return FALSE;
	}

	/* The coins file should only be saved if it was originally loaded from 
	 * there, else this will create errors */
	if ( system->coin_from_file == TRUE )
	{
		if ( save_Coins( system ) == FALSE )
		{
			printf("\n Failed to save to the Coin file : %s \n", 
							system->coin_file_name );
			return FALSE;
		}
	}
	/* As the data has been saved above, we can return FALSE, which will 
	 * trigger the abort_program function */
	return FALSE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when adding an item succeeds and false when it does not
 **/
BOOLEAN add_item( struct ppd_system * system )
{
	/* Create the memory space for the new item */
	struct ppd_stock * new_Item = malloc( sizeof( *new_Item ) );

	/* Input Buffers */
	char nameInput[ NAMELEN + EXTRACHARS ] = " ";
	char descInput[ DESCLEN + EXTRACHARS ] = " ";

	int intPrice = 0;

	BOOLEAN boolReturned = FALSE;
	
	/* Autogenerate Stock ID and assign it to the newItem->id */
	generateItemID( system, nameInput );
	strcpy( new_Item->id, nameInput );
	
	printf ( " Please enter the Name of the item you wish to Add : " );
	boolReturned = getText( nameInput, NAMELEN );

	/* User wants to quit */
	if ( boolReturned == FALSE )
	{
		/* Return true to simply return to the menu, without quitting the 
		 * program */
		free( new_Item );
		return TRUE;
	}
	
	strcpy( new_Item->name, nameInput );

	/* Reset the boolReturned variable */
	boolReturned = FALSE;

	printf ( " Please enter the Description of the item you wish to Add : " );
	boolReturned = getText( descInput, DESCLEN );

	/* User wants to quit */
	if ( boolReturned == FALSE )
	{
		/* Return true to simply return to the menu, without quitting the 
		 * program */
		free( new_Item );
		return TRUE;
	}

	strcpy( new_Item->desc, descInput );

	intPrice = getValidPrice ( );

	/* If the user wants to quit */
	if ( intPrice == -1 )
	{
		free( new_Item );
		return TRUE;
	}

	centsToPrice( intPrice, &new_Item->price );

	new_Item->on_hand = DEFAULT_STOCK_LEVEL;

	boolReturned = addToStockList( system->item_list, new_Item );
	if ( boolReturned == TRUE )
	{
		printf(" The %s has been successfully added to the stock list \n",
						 new_Item->name );   
	}
	else 
	{
		printf(" ERROR - The item was unable to be added to the stock list " );
		free( new_Item );
		return FALSE;
	}
	return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when removing an item succeeds and false when it does not
 **/
BOOLEAN remove_item( struct ppd_system * system )
{
	char nameInput[ NAMELEN + EXTRACHARS ] = " ";
	BOOLEAN boolReturned = FALSE;

	display_items( system );

	printf ( " Please enter the Name of the item you wish to remove : " );
	boolReturned = getText( nameInput, NAMELEN );

	/* If the user wants to quit, return TRUE, BEFORE deleting anything */
	if ( boolReturned == FALSE )
	{
		return TRUE;
	}
	
	removeFromStockList( system->item_list, nameInput );

	return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_stock( struct ppd_system * system )
{
	struct ppd_node * currentNode = system->item_list->head;

	/* Display the formatted output from each Node->data */
	while( currentNode != NULL )
	{
		currentNode->data->on_hand = DEFAULT_STOCK_LEVEL;

		/* Move onto the next node */
		currentNode = currentNode->next;
	}

	printf ( " All stock has been reset to the default level of %d \n",
						 DEFAULT_STOCK_LEVEL );

	return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_coins( struct ppd_system * system )
{
	int i;

	for ( i = 0; i < NUM_DENOMS; i++ )
	{
		system->cash_register[ i ].count = DEFAULT_COIN_COUNT;
	}

	return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail
 **/
BOOLEAN display_coins( struct ppd_system * system )
{
	int i;

	printf ( "\n   ---=== Coins Summary ===--- \n" );
	printf ( "------------------------------------ \n" );
	printf ( " %-*s | %s \n", 13, "Denomination", "Count");
	printf ( "------------------------------------ \n" );

	/* Display the formatted output for each denomination 
	 * NOTE this will print out each denomination in the order stored in the 
	 * array. It will check against the value stored and doesn't require the
	 * cash_register array to be sorted in a specific order */
	for ( i = 0; i < NUM_DENOMS; i++ )
	{
		/* Convert the current Denomination to Text and then print the result 
		 * NOTE this is used to help protect against denominations loaded in 
		 * the wrong order in the cash_register array */
		printf( " %-*s |", 13, denomToText( system->cash_register[ i ].denom ) );
		printf( " %d \n", system->cash_register[ i ].count );
	}
	printf("\n");

	/* As it is impossible for this function to fail */
	return TRUE;
}


/* Free any malloc'd memory and exit the program  */
/* To abort the program, we can simply return FALSE here, which will abort
 * the program by exiting the program loop in main.c and "free"ing memory
 * and exiting the program from there */
BOOLEAN abort_program ( struct ppd_system * system )
{
	/*system_free( system );

	return EXIT_SUCCESS;*/
	return FALSE;
}


