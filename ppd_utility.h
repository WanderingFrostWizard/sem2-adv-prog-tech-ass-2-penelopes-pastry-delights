/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#ifndef PPD_UTILITY
#define PPD_UTILITY

/* Also gives access to stock.h, coin.h, shared.h */
#include "ppd_main.h"

/**
 * @file ppd_utility.h defines some helper functions for the rest of your
 * program. In particular, it declares three of the most important 
 * functions for your application: @ref load_data which loads the data
 * in from files, @ref system_init which initialises the system to a 
 * known safe state and @ref system_free which frees all memory when you
 * are going to quit the application.
 **/

/**
 * the function to call for buffer clearing. This was discussed extensively
 * for assignment 1
 **/
void read_rest_of_line( void );

/**
 * Initialise the system to a known safe state. Before you post on the
 * discussion board about this, have a look at the structures as defined
 * in ppd_stock.h, ppd_coin.h and ppd_main.h. Given that nothing is 
 * initialized by the system if it
 * is a local variable, what would be good starting values for each of 
 * these. You already have some experience with this from assignment 
 * 1 to help you.
 **/
BOOLEAN system_init( struct ppd_system * );

/**
 * loads the stock file's data into the system. This needs to be 
 * implemented as part of requirement 2 of the assignment specification.
 **/
BOOLEAN load_stock( struct ppd_system *, const char * filename );

/* Saves the data from each item in the systems stock list to the file address
 * stored under stock_file_name */
BOOLEAN save_Stock( struct ppd_system * system );

/**
 * loads the contents of the coins file into the system. This needs to
 * be implemented as part 1 of requirement 18.
 **/
BOOLEAN load_coins( struct ppd_system *, const char * filename );

/* Saves the data from each denomination in the systems cash_register to the
 * file address stored under coin_file_name */
BOOLEAN save_Coins( struct ppd_system * system );

/**
 * free all memory that has been allocated. If you are struggling to
 * find all your memory leaks, compile your program with the -g flag
 * and run it through valgrind. An important thing to think about here:
 * as malloc() returns a memory address to the first byte allocated, you
 * must pass each of these memory addresses to free, and no other 
 * memory addresses.
 **/
void system_free( struct ppd_system * );

/* ### Adapted from my assignment 1 code ### */
/* Asks the user to enter an integer to select an option from the main menu,
 * it then validates that integer and if successful, returns it to the calling
 * function */
int getValidMenuNumber ( );

/* ### Adapted from my assignment 1 code ### */
/* This function is used to receive an ItemID, to allow the user to select an
 * item from the products menu or delete an item. It works by asking the user 
 * to enter an itemID, it then validates that string and if successful, returns
 * true to the calling function */
BOOLEAN getValidItemID ( char * input );

/* Adapted from my assignment 1 code */
/* This function asks the user to enter the amount of money inserted into the
 * machine (in cents). If this function fails it returns 0, if the user wants
 * to quit or due to serious failure, the function retuns -1, else it will 
 * return the number entered by the user. This function is also used to enter
 * a price when adding a new item to the system */
int getCents ( );

/* ### Adapted from my assignment 1 code ### */
/* This function is used to receive a product name and description from the 
 * user. It takes in a string, then validates it then returns it to the calling
 * function. */
BOOLEAN getText ( char * input, int maxLength );

/* Asks the user to enter a valid price and validates the input until correct,
 * or the user quits */
int getValidPrice ( );

/* Is used to check if the centsEntered represents a valid coin or note */
BOOLEAN isValidCentsValue ( int centsEntered, struct ppd_system * system );

/* Used to confirm if there is enough change to return to the customer and also
 * to return that change to the customer by deducting coins from the 
 * cash_Register. If the system can return the amount in change from the till,
 * this function will return true, else it will return false, and any change 
 * that has been counted out to the customer will be returned to the 
 * cash_Register */
BOOLEAN changeCountBack( int changeOwed, struct ppd_system * system );

/* Print out the coinsToReturn one at a time as a single string to simulate 
 * coins being return to the user */
void returnChange( BOOLEAN fromRegister, struct ppd_system * system,
									 int coinsGiven [] );

/* Checks through the stock List to find the longest name length and returns it
 * as an integer */
int getLargestNameLength( struct ppd_system * system );

/* Find the highest ID number within the item_list, then return this number + 1
 * in the correct format as a string */
char* findNextAvailableID( struct ppd_list * item_list, 
				struct ppd_node ** currentNode, struct ppd_node ** previousNode );

/* Used to autoGenerate the next avaialable Item ID Number 
char * generateItemID( struct ppd_system * system );*/
void generateItemID( struct ppd_system * system, char newID [] );

#endif
