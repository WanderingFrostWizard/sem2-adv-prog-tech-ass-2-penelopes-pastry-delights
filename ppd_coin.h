/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : BP215
 * Program Code     : COSC1076
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

/* Gives access to the BOOLEAN definition */
#include "ppd_shared.h"

/**
 * @file ppd_coin.h defines the coin structure for managing currency in the
 * system. You should declare function prototypes for managing coins here
 * and implement them in ppd_coin.c
 **/
#ifndef PPD_COIN
#define PPD_COIN

#define COIN_DELIM ","

#define SMALLEST_DENOM_CENTS 5
#define LARGEST_DENOM_CENTS 1000

/* The Maximum Length for a cents value */
#define CENTSLEN 4

/*struct ppd_system;    */                 
struct price;

/**
 * enumeration representing the various types of currency available in
 * the system. 
 **/
enum denomination
{
	FIVE_CENTS, TEN_CENTS, TWENTY_CENTS, FIFTY_CENTS, ONE_DOLLAR, 
	TWO_DOLLARS, FIVE_DOLLARS, TEN_DOLLARS
};

/**
 * represents a coin type stored in the cash register. Each demonination
 * will have exactly one of these in the cash register.
 **/
struct coin
{
	/**
	 * the denomination type
	 **/
	enum denomination denom;
	/**
	 * the count of how many of these are in the cash register
	 **/
	unsigned count;
};

/* Convert the integer value received to a price struct */
void centsToPrice( int centsOwed, struct price * convertedPrice );

/* Convert the enum received to an integer value */
int denomToCents( enum denomination coin_name );

/* Convert the integer value received to a denomination enum */
enum denomination centsToDenom( int * cents );

/* Checks to see if the coin is valid. Also increments the cash_Register
 * array. */
char* denomToText( enum denomination coin_name );

#endif
